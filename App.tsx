import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { StyleSheet, Text, View, SafeAreaView, Platform } from "react-native";
import DateIconComponent from "./components/DateIconComponent";

import "intl";
import "intl/locale-data/jsonp/en";
if (Platform.OS === "android") {
  // See https://github.com/expo/expo/issues/6536 for this issue.
  if (typeof (Intl as any).__disableRegExpRestore === "function") {
    (Intl as any).__disableRegExpRestore();
  }
}

export default function App() {
  const [content, setContent] = useState("Currently no date is selected");
  const updateContent = (text: string) => {
    setContent(text);
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.mainContainer}>
        <Text>{content}</Text>
        <DateIconComponent updateContent={updateContent}/>
      </View>
      <StatusBar style="auto" />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  mainContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    height: 350,
  },
});
