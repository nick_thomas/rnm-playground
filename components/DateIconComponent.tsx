import React from "react";
import { DatePickerModal } from "react-native-paper-dates";
import { StyleSheet, View, TextInput } from "react-native";
import { Icon } from "react-native-elements";

interface PropInterface {
  updateContent: (text: string) => void;
}

const DateIconComponent = (props: PropInterface) => {
  let { updateContent } = props;
  const [date, setDate] = React.useState<Date | undefined>(undefined);
  const [open, setOpen] = React.useState(false);
  const [text, onChangeText] = React.useState("Useless Text");

  const onDismissSingle = React.useCallback(() => {
    setOpen(false);
  }, [setOpen]);

  const onConfirmSingle = React.useCallback(
    (params) => {
      setOpen(false);
      setDate(params.date);
      updateContent(params.date.toLocaleString());
    },
    [setOpen, setDate]
  );
  return (
    <View style={styles.container}>
      <Icon
        reverse
        onPress={() => setOpen(true)}
        name="calendar"
        type="font-awesome"
      />

      <DatePickerModal
        // locale={'en'} optional, default: automatic
        mode="single"
        visible={open}
        onDismiss={onDismissSingle}
        date={date}
        onConfirm={onConfirmSingle}
        // validRange={{
        //   startDate: new Date(2021, 1, 2),  // optional
        //   endDate: new Date(), // optional
        // }}
        // onChange={} // same props as onConfirm but triggered without confirmed by user
        // saveLabel="Save" // optional
        // label="Select date" // optional
        // animationType="slide" // optional, default is 'slide' on ios/android and 'none' on web
      />
      <View style={styles.inputZone}>
        <View style={styles.viewReset}>
          <TextInput
            style={styles.input}
            onChangeText={onChangeText}
            value={text}
          />
        </View>
        <View style={styles.viewReset}>
          <Icon
            containerStyle={styles.iconPlacement}
            onPress={() => setOpen(true)}
            name="calendar"
            type="font-awesome"
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#eee",
    alignItems: "center",
    justifyContent: "center",
  },
  inputZone: {
    flexDirection: "row",
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
  },
  iconPlacement: {
    marginTop:12
  },
  viewReset: {
    flex: 1,
  },
});

export default DateIconComponent;
